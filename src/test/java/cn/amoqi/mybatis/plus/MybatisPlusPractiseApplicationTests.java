package cn.amoqi.mybatis.plus;

import cn.amoqi.mybatis.plus.api.entity.Student;
import cn.amoqi.mybatis.plus.api.mapper.StudentMapper;
import cn.amoqi.mybatis.plus.api.service.IStudentService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisPlusPractiseApplicationTests {

    @Autowired
    private IStudentService studentService;

    @Test
    public void add(){
        Student student = new Student();
        student.setName("赵六");
        student.setAge(23);
        student.setAddress("广东");
        studentService.save(student);
    }

    @Test
    public void edit(){
        Student student = new Student();
        student.setName("赵六111");
        student.setId(1288121141721862146L);
        studentService.updateById(student);
    }

    @Test
    public void del(){
        studentService.removeById("1288121141721862146");
    }

    @Test
    public void list(){
        List<Student> list = studentService.list(Wrappers.<Student>lambdaQuery()
                .ge(Student::getAge, 5));
        System.out.println(list);
        //复杂的
        /*SELECT id,name,age,address FROM student WHERE name = ? AND ( age = ? OR address = ? ) */
        List<Student> list1 = studentService.list(Wrappers.<Student>lambdaQuery().eq(Student::getName, "张三")
                .and(wrapper -> wrapper.eq(Student::getAge, 3).or().eq(Student::getAddress, "山东")));
        System.out.println(list1);
    }

    @Test
    public void pageList(){
        IPage<Student> page = studentService.page(new Page<Student>(1, 10),
                Wrappers.<Student>lambdaQuery().ge(Student::getAge, 3));
        System.out.println(page.getRecords());
    }
    @Test
    public void pageList2(){
        IPage<Student> studentIPage = studentService.selectListWithSchool(1);
        System.out.println(studentIPage.getRecords());
    }

}
