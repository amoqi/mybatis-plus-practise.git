package cn.amoqi.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.amoqi.mybatis.plus.api.mapper")
public class MybatisPlusPractiseApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusPractiseApplication.class, args);
    }
}