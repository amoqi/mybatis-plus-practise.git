package cn.amoqi.mybatis.plus.api.service;

import cn.amoqi.mybatis.plus.api.entity.Student;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lemon
 * @since 2020-07-27
 */
public interface IStudentService extends IService<Student> {

    IPage<Student> selectListWithSchool(Integer page);
}
