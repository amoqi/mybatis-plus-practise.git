package cn.amoqi.mybatis.plus.api.service.impl;

import cn.amoqi.mybatis.plus.api.entity.Student;
import cn.amoqi.mybatis.plus.api.mapper.StudentMapper;
import cn.amoqi.mybatis.plus.api.service.IStudentService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lemon
 * @since 2020-07-27
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    @Override
    public IPage<Student> selectListWithSchool(Integer page){
        Page<Student> studentPage = new Page<Student>(page, 10);
        IPage<Student> studentIPage = this.baseMapper.selectListWithSchool(studentPage);
        return studentIPage;
    }
}
