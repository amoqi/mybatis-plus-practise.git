package cn.amoqi.mybatis.plus.api.mapper;

import cn.amoqi.mybatis.plus.api.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lemon
 * @since 2020-07-27
 */
public interface StudentMapper extends BaseMapper<Student> {
    @Select("select st.*,sc.name school_name from student st left join school sc on st.school_id = sc.id")
    IPage<Student> selectListWithSchool(Page page);
}
