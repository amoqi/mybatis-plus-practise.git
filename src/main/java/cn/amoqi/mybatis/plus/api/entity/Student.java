package cn.amoqi.mybatis.plus.api.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author lemons
 * @since 2020-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    //IdType.AUTO、IdType.UUID、IdType.ID_WORKER_STR
    @TableId(type = IdType.ID_WORKER)
    private Long id;

    private String name;

    private Integer age;


    private String address;

    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date update_time;

    @TableLogic(delval = "0",value = "1")
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

    @Version
    private Integer version;

    private Integer schoolId;

    @TableField(exist = false)
    private String schoolName;
}
