package cn.amoqi.mybatis.plus.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
@Order(1)
public class AfterStartRun implements ApplicationRunner {

    @Value("${server.port}")
    private String port;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("端口号是：{}",port);
        log.info("获取命令行中的所有参数：{}",Arrays.toString(args.getSourceArgs()));
        log.info("可以用来获取命令行中的无key参数（和CommandLineRunner一样）:{}",args.getNonOptionArgs());
        log.info("可以用来获取所有key/value形式的参数的key：{}",args.getOptionNames());
    }
}
