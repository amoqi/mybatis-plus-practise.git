# Mybatis Plus

mybatis plus 是一款mybatis增强插件，可以在mybatis基础上来进行灵活的单表操作，不用再写SQL语句（仅限于单表），是一款简单易用的工具，本文结合SpringBoot来对Mybatis plus进行一个详细的讲解。
撒旦法第三方34343
啊速度发多少6445454
阿斯蒂芬
发生的
2343

## 1、初始化

1.1 选择一个数据库，然后创建表

```sql
CREATE TABLE `student`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `version` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
```

1.2 使用IDEA来创建SpringBoot项目

![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/225325_5fa50ace_1527141.png "image-20200730085309430.png")

1.3 引入POM依赖

```xml
 <!--mysql-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <!--druid-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>1.1.14</version>
        </dependency>
        <!--mybatis plus-->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.1.1</version>
        </dependency>
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-generator</artifactId>
            <version>3.1.1</version>
        </dependency>
        <!--lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.6</version>
            <scope>provided</scope>
        </dependency>
        <!--freemarker对自动生成代码支持-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-freemarker</artifactId>
            <version>2.1.3.RELEASE</version>
        </dependency>
```

1.4 修改配置文件application.yml

```yml
spring:
  application:
    name: mybatis-plus-practise
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai
    username: root
    password: 123456
  jackson:
    time-zone: GMT+8
    date-format: yyyy-MM-dd HH:mm:ss
server:
  port: 8080
logging:
  level:
    cn.amoqi.mybatis.plus : debug
    org.springframework: warn
mybatis-plus:
  # mybatis的xml文件位置
  mapper-locations: classpath:/mapper/**/*Mapper.xml
  # entity实体类包位置
  typeAliasesPackage: cn.amoqi.mybatis.plus.api.entity
  global-config:
    db-config:
      # 数据库主键生成方式:AUTO：数据库自增，ID_WORKER:雪花算法生成(框架默认),
      #可在字段上加@TableId(type = IdType.ID_WORKER) 覆盖
      id-type: AUTO
      # 影响添加、更新、Wapper生成条件，not_empty字段为空的不进行操作，IGNORED忽略判断
      #可在字段上加@TableField(strategy = FieldStrategy.IGNORED)覆盖默认
      field-strategy: not_empty
      # 数据库字段强行按照java实体的骆驼式命名法大写字母前转化为下划线加小写
      column-underline: true
      # 逻辑删除有效为1 删除为0
      logic-not-delete-value: 1
      logic-delete-value: 0
    # 是否自动刷新 Mapper 对应的 XML 文件,
    # Mapper 对应的 XML 文件会自动刷新，更改 XML 文件后，无需再次重启工程，由此节省大量时间。
    refresh: true
  configuration:
    # 开启驼峰，数据库字段转换为驼峰命名
    map-underscore-to-camel-case: true
    cache-enabled: false
```

使用`CodeGenerator`类来生成controller、service、entity、mapper等，因为篇幅原因暂时就不列出代码生成器了，代码会传到gitee上，地址放到底部，需要的可以自取。

1.5 为启动类添加注解@MapperScan注解，指向mapper接口路径

```
@SpringBootApplication
@MapperScan("cn.amoqi.mybatis.plus.api.mapper")
public class MybatisPlusPractiseApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusPractiseApplication.class, args);
    }
}
```

1.6 目录如下

![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/225352_8567111d_1527141.png "image-20200225181646069.png")

## 2、基本功能

### 2.1 添加

```java
@Test
public void add(){
    Student student = new Student();
    student.setName("赵六");
    student.setAge(23);
    student.setAddress("广东");
    studentService.save(student);
}
```

### 2.2 修改

```java
@Test
public void edit(){
    Student student = new Student();
    student.setName("赵六111");
    student.setId(1288121141721862146L);
    studentService.updateById(student);
}
```

### 2.3 删除

```java
@Test
public void del(){
	studentService.removeById("1288121141721862146");
}
```

### 2.4 列表

```java
@Test
public void list(){
    List<Student> list = studentService.list(Wrappers.<Student>lambdaQuery()
    .ge(Student::getAge, 5));
    System.out.println(list);
    //复杂的
    /*SELECT id,name,age,address FROM student WHERE name = ? AND ( age = ? OR address = ? ) */
    List<Student> list1 = studentService.list(Wrappers.<Student>lambdaQuery().eq(Student::getName, "张三")
    .and(wrapper -> wrapper.eq(Student::getAge, 3).or().eq(Student::getAddress, "山东")));
    System.out.println(list1);
}
```

### 2.5 分页

引入Mybatis plus分页插件

```java
@EnableTransactionManagement
@Configuration
public class MyBatisPlusConfig {
    /**
     * mybatis-plus配置
     */
    public class MybatisPlusConfig {
        /**
         * 分页插件
         */
        @Bean
        public PaginationInterceptor paginationInterceptor() {
            return new PaginationInterceptor();
        }
    }
}
```

分页测试1

```java
    @Test
    public void pageList(){
        IPage<Student> page = studentService.page(new Page<Student>(1, 10),
                Wrappers.<Student>lambdaQuery().ge(Student::getAge, 3));
        System.out.println(page.getRecords());
    }
```

复杂分页查询，使用xml关联别的表，查询列表分页的情况

创建一个`school`表

```sql
CREATE TABLE `school`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of school
-- ----------------------------
INSERT INTO `school` VALUES (1, '明天中学');
```

为Student实体类中添加

```java
private Integer schoolId;

@TableField(exist = false)
private String schoolName;
```

mapper

```java
public interface StudentMapper extends BaseMapper<Student> {
    @Select("select st.*,sc.name school_name from student st left join school sc on st.school_id = sc.id")
    IPage<Student> selectListWithSchool(Page page);
}
```

service

```java
@Override
public IPage<Student> selectListWithSchool(Integer page){
    Page<Student> studentPage = new Page<Student>(page, 10);
    IPage<Student> studentIPage = this.baseMapper.selectListWithSchool(studentPage);
    return studentIPage;
}
```

分页测试2

```java
@Test
    public void pageList2(){
        IPage<Student> studentIPage = studentService.selectListWithSchool(1);
        System.out.println(studentIPage.getRecords());
    }
```

## 3、其他

### 3.1 逻辑删除

配置mybatis-plus

```yaml
mybatis-plus:
  global-config:
    db-config:
      logic-delete-field: flag  # 全局逻辑删除的实体字段名(since 3.3.0,配置后可以忽略不配置步骤2)
      logic-delete-value: 1 # 逻辑已删除值(默认为 1)
      logic-not-delete-value: 0 # 逻辑未删除值(默认为 0)
```

在实体类字段上加上

```java
@TableLogic
private Integer deleted;
```

### 3.2 生成主键

```yml
mybatis-plus:
  global-config:
    db-config:
      # 数据库主键生成方式:AUTO：数据库自增，ID_WORKER:雪花算法生成(框架默认),
      #可在字段上加@TableId(type = IdType.ID_WORKER) 覆盖
      id-type: AUTO
```

### 3.3 自动填充功能

实现元对象处理器接口：com.baomidou.mybatisplus.core.handlers.MetaObjectHandler

```java
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");  //日志
        this.setFieldValByName("create_time",new Date(),metaObject);
        this.setFieldValByName("update_time",new Date(),metaObject);
        this.setFieldValByName("status",1,metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        this.setFieldValByName("update_time", new Date(), metaObject);
    }
}
```

字段上加入

```java
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    @TableField(fill = FieldFill.INSERT_UPDATE)
```



